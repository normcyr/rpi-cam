# Raspberry Pi security camera

## Description

This project will be about building a security camera and controlling it with a Raspberry Pi. Largely inspired by a tutorial on PiMyLifeUp website (see http://pimylifeup.com/raspberry-pi-security-camera/). The ultimate objective will be to use it as a surveillance camera to spot dears in the woods prior to the hunting season.

## Milestones

1. Gather all the necessary parts
2. Setup the Raspberry Pi with MotionEyeOS
3. Setup the camera to function with the Pi
4. Record short video clips at defined intervals
5. Setup a motion detector to trigger the camera
6. Weatherproof the rig
7. Field test using the motion detector

## Necessary parts

* Raspberry Pi model A+ board (any model would do) http://www.robotshop.com/ca/en/raspberry-pi-model-a-plus-computer-board.html
* RPi NoIR camera http://www.robotshop.com/ca/en/raspberry-pi-infrared-camera-module-noir.html
* MicroSD card, 16GB http://www.amazon.ca/gp/product/B010Q57SEE/
* USB WiFi dongle http://www.robotshop.com/ca/en/usb-wifi-adapter-wireless-n-80211n.html
* 2 8 x AA 12V Battery Holder Case Box http://www.amazon.ca/Battery-Holder-Wired-Switch-Cover/dp/B00H8WJ1SA
* 25 IR LEDs pack http://www.robotshop.com/ca/en/sfe-ir-850-nm-led-25-pack.html

## Setup the Raspberry Pi

## Inspirations/bibliography/references

### Projects

* Original idea http://pimylifeup.com/raspberry-pi-security-camera/
* Pulsed IR illuminator http://www.lucidscience.com/pro-pulsed%20led%20illuminator-1.aspx
* Simple IR illuminator http://www.lucuidscience.com/pro-simple%20infrared%20illuminator-1.aspx

### Products

* Night vision security camera http://www.amazon.com/HAMSWAN%C2%AE-K802-Vision-Security-Camera/dp/B00JB5JME0/
* Raspberry Pi model A+ http://www.robotshop.com/ca/en/raspberry-pi-model-a-plus-computer-board.html
* RPi NoIR camera http://www.robotshop.com/ca/en/raspberry-pi-infrared-camera-module-noir.html

###

* MotionEyeOS, forked from Calin Crisan's project https://gitlab.com/normcyr/motioneyeos
